# Level 5: Larger plaintext input
The previous level got to a point where brute-forcing by hand became infeasible, but a clever cracker could write some computer program to still brute force the answer. However, when brute forcing it that way, the way an attacker would know they landed on the correct decryption key is if the decoded text looks like an English message. This was how the Enigma machine encryption was first cracked; if Eve knows that the phrase "weather report" is likely to be in a given message, they can write a computer program to try all the permutations until "weather" or "report" appears int he decrypted text.

For more advanced use, to thwart that sort of brute-forcing (and to allow the possibility of encoding numbers and punctuation!), we can first change our plaintext message in a way that it no longer looks like English, by taking the 26-letter-alphabet and expanding it with a [straddling checkerboard](http://www.practicalcryptography.com/ciphers/straddle-checkerboard-cipher/). Most straddling checkerboards use just the digits 0-9 as possible values, but with the Super Cipher Wheel, we have 1-26 as possibilities, so we can create a much wider checkerboard than normal. To create a straddling checkerboard, pick a password/passphrase to start the checkerboard. Write it out, skipping any letters that repeat, and then fill in the rest of the alphabet after leaving a blank in the checkerboard.

For example, Bob and Alice agree on the following straddling checkerboard for their communication. It uses the word `ZEBRA` as the key, to make it easier to recreate this key from memory.

|     | 1 |  2  | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 |
|----:|---|:---:|---|---|---|---|---|---|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
| x:  | Z |  E  | B | R | A |   | C | D | F |  G |  H |  I |  J |  K |  L |  M |  N |  O |  P |  Q |  S |  T |  U |  V |  W |  X |
| 6x: | Y | `Sp`| 0 | 1 | 2 | 3 | 4 | 5 | 6 |  7 |  8 |  9 |  . |  , |  ' |  ! |  ? |  🙂 |  🙁 |  😉 |  ❤️ |  ⭐️ |  ☀️ |  ⛅️ |  🌧 |  ⛈ |

Straddling checkerboards need at least one space in the first row to be blank to serve as a flag to look up the value on a different row. Putting a blank space after the password/phrase (assuming the phrase is less than 26 characters long) provides that blank space (Note that on a straddling checkerboard key like this chart, "Sp" means a space/word-break, while an empty square means "drop down to a lower row"). This chart then had 24 spaces that Alice and Bob be filled in with numeric digits, punctuation, and some common emojis that could represent different feelings or concepts. They could also have used the extra slots as blank spots to create additional rows of the straddling checkerboard if they wanted even more options.

To use this method, first the plain text message is changed into a series of numbers (technically called "fractionating" the text). Note that two-digit numbers in the table (e.g. "14") are not encoded as two digits, but will be represented by a letter, representing that number.

So, for Bob to send the message "Hello, friend!", using the chart he and Alice agreed to, he would first convert it to numbers like:

|  H | e | l  | l  | o  | ,    |     | f | r | i  | e | n  | d | ! |
|----|---|----|----|----|------|-----|---|---|----|---|----|---|------|
| 11 | 2 | 15 | 15 | 18 | 6 14 | 6 2 | 9 | 4 | 12 | 2 | 17 | 8 | 6 16 |

Note how coming across a "6" in the stream means to interpret the next number in the stream as belonging to the second row, not the first.

To encode this message, the Super Cipher Wheel has the numbers 1-26 on the outer ring alongside the letters A-Z. Use the letters for the encoded number stream as the letters to encode via whichever method you'd like.

Using that method, the message Bob would encode would be `KBOORFNFBIDLBQHFP`, using any of the previously-described encryption methods. Once Alice decodes the message, she'll have `KBOORFNFBIDLBQHFP`, and will be able to work it backward through the straddling checkerboard. The benefit of this method is that if Eve tried to brute-force the decryption method, every option would result in a "decrypted" message that looked like random garbage, since `KBOORFNFBIDLBQHFP` doesn't _look_ like it's the correct message just looking at it. Only if Eve figures out the Super Cipher Wheel process **AND** the passphrase of the straddling checkerboard would they be able to decipher the message.

Another example of creating a straddling checkerboard, using "I Love Donuts" as the 
key phrase:

|     | 1 |  2  | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 |
|----:|---|:---:|---|---|---|---|---|---|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
| x:  | I |   | L | O | V | E |   | D | N | U | T | S |   | A | B | C | F | G | H | J | K | M | P | Q | R | W |
| 2x: | X | Y | Z | `Sp`| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | . | , | ' | ! | ? | 🙂 | 🙁 | 😉 | ❤️ | ⭐️ | Yes | No |
| 7x: | ☀️ | ⛅️ | 🌧 | ⛈ |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |


| 13x: | Value |
| --- | --- |
| 1 | Alice |
| 2 | Bob |
| 3 | Eve |
| 4 | Yesterday |
| 5 | Tomorrow |
| 6 | Clubhouse |
| 7 | After |
| 8 | Before |
| 9 | Stop |

Note that the "O" in "DONUTS" is not used, since there is already an "O" in "LOVE". With this checkerboard, there's several more rows available to fill in, which can be used to encode common words with just one number. This sort of checkerboard is less likely to be memorized, but if two message-senders end up using certain words frequently, and can keep a printed reference to their agreed-upon checkerboard safe, it can make encoding/decoding take less time.