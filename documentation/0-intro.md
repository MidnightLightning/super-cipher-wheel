# Guide
We're going to lay out five levels of increasing complexity to use the Super Cipher Wheel for, which result in more and more difficult-to-crack messages. We'll give examples of each method along the way, using a message of "Hello World" that Bob wants to send to Alice without Eve figuring it out.

The wheel has just 26 slots, such that at the basic level it can operate as a standard "decoder ring" (able to encode A-Z only; not able to encode spaces, punctuation, or numbers), and acts as a one-rotor Enigma machine.

It has a "rotor" like the Enigma machine has, and ends with a "reflector" ring as the inner ring. The "rotor" ring is a half-ring and a full-ring, each with numbers 1-13 on them (colored blue and orange to differentiate). They can be rotated against each other to modify the "wiring" of the rotor. The "reflector" ring has the numbers 1-13, repeated twice, with one set having a red background and the other a green background, to differentiate them.