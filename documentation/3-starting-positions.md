# Level 3: Maximizing starting positions
The weakness of the previous method is that the inner ring only has 26 positions it can be in, making brute-forcing the solution a possibility. To fix this, we'll now engage the use of the middle ring and slightly alter our encoding method. The inner ring scrambles the input and output by "wiring" two positions together. This is similar to a "rotor" used in an Enigma machine, and like an Enigma rotor, having it rotate for each letter of the message helps add complexity to the message. But the "wiring" of that inner ring is hard-coded (for example, the two number "1"s are right next to each other; in all cases where that slot of the rotor is used, it always only redirects the input one slot over). If we wanted a different scrambling of the positions of the numbers around that ring, we'd have to create a separate physical ring and swap it out. However, the middle ring was designed to allow not only changing the orientation of the rotor in relation to the other rings, but also to be able to re-wire the relationship between the pairs of numbers on it. The middle ring is constructed of a half-ring that's overlaid over a full-ring. The numbering of the full ring is such that no matter which slots the half-ring is covering up, you can still see the digits 1-13 visible in the half that remains. So, rotating the half-ring in respect to the full ring re-wires the rotor to a new combination, making it able to represent 13 different wiring combinations in one physical rotor.

To encode a letter using all three rings:

- Find the letter on the outer ring
- Look at the number on the middle ring aligned with it, and find the same number on the other half of the middle ring (with a different background color).
- Look at the number on the inner ring number aligned with that, and find the same number on the inner ring with the other background color.
- Look back up to the middle ring number aligned to that, and find the same number on the other half of the middle ring (with a different background color).
- Look up to the outer ring, and note the letter aligned there.

That final letter is the encrypted letter. Note that you do a "down, and back up again" action, using the different colors to jump to a different position on each ring before dropping down/up to the next ring. The decryption process is exactly the same as the encryption process.

In previous methods, the setup only could be done just with knowing the name of the person sending the message. With this method, sender and recipient need to exchange three different "Key" values, which will be used to set the initial position of the Super Cipher Wheel.

To add more combinations of possibilities, we'll also alter what happens between each letter encoding of the message. Instead of just rotating the inner ring by one slot, we'll rotate it by some random amount between one and four, and we'll rotate the half-ring of the middle ring by some random amount too.

So, to encode a message there's several "key" values that we'll use:

| Name | Value | Purpose
| --- | --- | --- |
| Key 1 | 1-26 | Initial position of inner ring
| Key 2 | 1-13 | Initial position of middle full-ring
| Key 3 | 1-26 | Initial position of middle half-ring
| Key 4 | 1-4  | How many rotation steps for the inner ring to move between each letter
| Key 5 | 1-4  | How many rotation steps for the middle half-ring to move between each letter

Before two individuals start communicating, they pick a value for Keys 1, 2, and 3 that they will always use, and share them secretly with each other. Then, for each message they send to each other:

- Set the wheel to an initial orientation with the outer ring having the first letter of your first name at the top, the middle full-ring having the orange "1" aligned with that key letter in the outer ring, and the inner ring having the red "1" aligned with that slot. Then, put the middle half-ring on the left side of the ring, right up to the orange "1" on the middle full-ring (such that rotating the half-ring one more slot clockwise would cover up the orange "1" on the middle full-ring). This is the neutral starting point
- If "Key 1" is 13 or less, rotate the inner ring until the "Key 1" number with the red background is aligned with the first letter of your first name in the outer ring. If "Key 1" is greater than 13, rotate the inner ring until the number with the green background equal to "Key 1" minus 13 is aligned with the first letter of your first name in the outer ring.
- Rotate the middle full-ring until the "Key 2" value is aligned to the first letter in your first name in the outer ring. (don't rotate the inner ring nor the middle half-ring as you do so; the "Key 1" number should now be aligned with the "Key 2" number).
- If "Key 3" is 13 or less, rotate the middle half-ring clockwise until the "Key 3" value in the middle full-ring is just about to be covered. If "Key 3" is greater than 13, rotate the middle half-ring counter-clockwise until the number with the orange background equal to "Key 3" minus 13 is just exposed. This is now the starting position for this particular message
- Pick a random number 1-4 to be "Key 4" for this message. Find the "Key 4" number in the outer ring and encode that letter as the first character of your message. Rotate the inner ring clockwise "Key 4" number of slots.
- Pick a random number 1-4 to be "Key 5" for this message. Find the "Key 5" number in the outer ring and encode that letter as the second character of your message. Rotate the inner ring clockwise "Key 4" number of slots, and then rotate the middle half-ring clockwise "Key 5" number of slots.
- Encode the first letter of your message. Then rotate the inner ring clockwise "Key 4" number of slots, and rotate the middle half-ring clockwise "Key 5" number of slots.
- Repeat for the rest of the message, rotating the inner ring clockwise "Key 4" number of slots, and the middle half-ring clockwise "Key 5" number of slots between each letter.

That's a lot of explanation; let's use some examples to make it clearer: Bob is again wanting to send the message of "Hello World" to Alice. To set up, he not only needs the first letter of his first name ("B"), but also several "key" numbers:

| Name | Value |
| --- | --- |
| Key 1 | 17 |
| Key 2 | 5 |
| Key 3 | 3 |
| Key 4 | 2 |
| Key 5 | 4 |

The first three keys Alice and Bob share with each other when they know Eve's not listening in, and keep them super-secret.

![](img/1_Bob_start.png)

After setting up his Super Cipher Wheel in the starting position (the orange 1 in the middle track and the red 1 in the inner track both under the "B" of the outer track, and the blue 6 of the middle track under the "A" of the outer track), Bob will use the "Key 1" value to set the starting position for the inner ring. Because his "Key 1" value is greater than 13, it will be a green background number. 17 - 13 = 4, so he rotates the inner ring until the green 4 is under the "B" of the outer track.

Then he will use the "Key 2" value to set the position of the middle full-ring, by turning it until a 5 is in line with the "B" on the outer track (keeping the middle half-ring where it is). So, now the "B" in the outer ring is aligned with the orange 5 and the green 4. The "A" in the outer ring is still in line with the blue 6.

![](img/3a_Bob.png)

After that, the "Key 3" is used to set the position of the middle half-ring. He rotates the middle half-ring clockwise until the orange 3 is just about to be covered up. Now, the "B" is aligned with the blue 11 and the green 4. And, the "M" is aligned with the orange 3 and green 12.

![](img/3b_Bob.png)

Next, he must encode the "Key 4" value (so Alice knows how to decode this particular message), which is 2. So, Bob encodes a "B" by seeing the "B" in the outer ring is in line with the blue 11, so he finds the orange 11. That is in line with the green 2, so he looks for the red 2. It is in line with the blue 12, so he looks where the orange twelve is in line with, and sees it's a "V". After that, he rotates the inner ring two steps clockwise.

![](img/3c_Bob.png)

Finally the "Key 5" value, which is 4. That's a "D" on the outer ring, so becomes "Q" when encoded. Bob then rotates the inner ring 2 steps clockwise, and the middle half-ring 4 steps clockwise.

![](img/3d_Bob.png)

After all that, Bob has encoded his random Key values for this particular message, and he's ready to encode the actual message to Alice. His Super Cipher Wheel has the "B" aligned with the orange 5 and the red 4, and the "P" is aligned with the blue 6 and red 11.

The first letter of his message is "H", which gets encoded as a "P". Then Bob rotates the inner ring 2 steps clockwise, and the middle half-ring 4 steps clockwise. The next letter is "E", which gets encoded as a "V". Then Bob rotates the inner ring 2 steps clockwise, and the middle half-ring 4 steps clockwise, and continues the process.

In the end, `HELLO WORLD` becomes `VQPVPND PINBF`, which he can give to Alice.

Using this method, we've gone from having just 26 possible encoding methods to having 26 (inner ring positions) &times; 13 (middle full-ring positions) &times; 26 (middle half-ring positions). That's 8,788 possibilities which Eve would need to brute force. If Eve has a copy of this guide, and therefore knows how Alice and Bob are encoding the rotation values, those are all the possibilities she'd need to brute-force. Adding complexity to that process is the next level: